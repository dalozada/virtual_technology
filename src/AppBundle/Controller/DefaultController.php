<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        
        return $this->render('base.html.twig');
    }
    

    
    //funcionalidad que llama al repositorio del Menu para consultar los menus y renderizarlos 
    public function listarMenuAction()
    {
        $em = $this->getDoctrine()->getManager();     
        $menu = $em->getRepository('AppBundle:Menu')->findMenus();
        return $this->render('_menu.html.twig',array('menus'=>$menu));
    }
     
}
