<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\UsuarioRegistro;
use AppBundle\Entity\Persona;

/**
 * Description of MenuController
 * @Route("/menu")
 * @author dfmarin
 */
class MenuController extends Controller {
    //put your code here

    /**
     * @Route("/prueba1", name="prueba1")
     */
    public function prueba1Action(Request $request) {
        // replace this example code with whatever you need

        return $this->render('menus/pruebas/prueba1.html.twig');
    }

    /**
     * @Route("/prueba2", name="prueba2")
     */
    public function prueba2Action(Request $request) {
        // replace this example code with whatever you need

        return $this->render('menus/pruebas/prueba2.html.twig');
    }

    /**
     * @Route("/registrarUsuario", name="registrarUsuario")
     */
    public function registrarUsuarioAction(Request $request) {
        // replace this example code with whatever you need
        
        $user= $this->listarPersonas();
        $usuarioRegistro = new UsuarioRegistro();
        $form = $this->createForm('AppBundle\Form\UsuarioType', $usuarioRegistro);
        $form->handleRequest($request);

        if ($form->isValid()) {//NO ESTA FUNCIONANDO EL ISVALID NUNCA ENTRA AL PARECER
            $persona = new Persona();
            $persona->setNombre1($usuarioRegistro->getNombre1());
            $persona->setNombre2($usuarioRegistro->getNombre2());
            $persona->setApellido1($usuarioRegistro->getApellido1());
            $persona->setApellido2($usuarioRegistro->getApellido2());
            $persona->setDireccion($usuarioRegistro->getDireccion());
            $persona->setEmail($usuarioRegistro->getEmail());
            $persona->setFechaNac($usuarioRegistro->getFechaNac());
            $persona->setIdentificacion($usuarioRegistro->getIdentificacion());
            //$persona->setFechaNac(date_create("1994-01-31"));


            $em = $this->getDoctrine()->getManager();
            $c = $em->getRepository('AppBundle:Ciudad')->findBy(['codigo' => $usuarioRegistro->getCiudadNac()]);
            $persona->setciudadNac($c[0]);
            $c = $em->getRepository('AppBundle:Ciudad')->findBy(['codigo' => $usuarioRegistro->getCiudadRes()]);
            $persona->setciudadRes($c[0]);
            $em->persist($persona);
            $em->flush();
            return $this->render('menus/usuario/registro.html.twig',  ['formulario' => $form->createView(),'user' => $user]);
        }


    return $this->render('menus/usuario/registro.html.twig', ['formulario' => $form->createView(),'user' => $user]);
    }
       
    
    public function listarPersonas()
    {
        $em = $this->getDoctrine()->getManager();     
        $user = $em->getRepository('AppBundle:Persona')->findAll();
        return $user;
    }

    /**
     * @Route("/prueba", name="prueba")
     */
    public function pruebaAction(Request $request) {
        // replace this example code with whatever you need

        return $this->render('menus/pruebas/prueba.html.twig');
    }

}
