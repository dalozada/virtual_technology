<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of MenuRepository
 *
 * @author dfmarin
 */
class MenuRepository extends EntityRepository {

    public function findMenus() {

        $em = $this->getEntityManager();
        $dql = 'SELECT m FROM AppBundle:Menu m ';
        $consulta = $em->createQuery($dql);
        return $consulta->getResult();
    }
    public function findPersonas() {

        $em = $this->getEntityManager();
        $dql = 'SELECT u FROM AppBundle:Usuario u ';
        $consulta = $em->createQuery($dql);
        return $consulta->getResult();
    }
    
    

}
