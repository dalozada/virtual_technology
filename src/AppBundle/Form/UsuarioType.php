<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use AppBundle\Entity\UsuarioRegistro;

/**
 * Description of PersonaForm
 *
 * @author dfmarin
 */
class UsuarioType extends AbstractType {

    //put your code here
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
                ->add('identificacion', TextType::class, [ 'attr' => ['placeholder' => 'identificacion','novalidate' => 'novalidate' ],
                    'constraints' => [
                        new NotBlank(["message" => "Por favor, ingresa tu identificacion"]),
                    ]
                ])
                ->add('nombre1', TextType::class, ['attr' => array('placeholder' => 'Primer nombre', 'novalidate' => 'novalidate'),
                ])
                ->add('nombre2', TextType::class, ['attr' => array('placeholder' => 'Segundo nombre'),
                ])
                ->add('apellido1', TextType::class, ['attr' => array('placeholder' => 'Primer apellido'),
                ])
                ->add('apellido2', TextType::class, ['attr' => array('placeholder' => 'Segundo apellido'),
                ])
                ->add('fechaNac', BirthdayType::class)
                ->add('direccion', TextType::class, ['attr' => array('placeholder' => 'direccion'),
                ])
                ->add('email', EmailType::class, ['attr' => array('placeholder' => 'correo electronico'),
                ])
                ->add('ciudadNac')
                ->add('ciudadRes')
                ->add('usuario')
                ->add('registrarme', SubmitType::class)
        ;
    }

   
     public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UsuarioRegistro::class,
            'csrf_protection' => false,
            "allow_extra_fields" => true
        ]);
    }
    public function getBlockPrefix() {
        return 'usuario_form';
    }

}
