<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Submenu
 *
 * @ORM\Table(name="submenu")
 * @ORM\Entity
 */
class Submenu
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_submenu", type="string", length=100, nullable=true)
     */
    private $nombreSubmenu;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=100, nullable=true)
     */
    private $url;

    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Menu", inversedBy="smenus")
    * @ORM\JoinColumn(name="idmenu", referencedColumnName="id")
    */
    private $menu;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombreSubmenu
     *
     * @param string $nombreSubmenu
     *
     * @return Submenu
     */
    public function setNombreSubmenu($nombreSubmenu)
    {
        $this->nombreSubmenu = $nombreSubmenu;

        return $this;
    }

    /**
     * Get nombreSubmenu
     *
     * @return string
     */
    public function getNombreSubmenu()
    {
        return $this->nombreSubmenu;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Submenu
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set menu
     *
     * @param integer $menu
     *
     * @return Submenu
     */
    public function setMenu($menu)
    {
        $this->menu = $menu;

        return $this;
    }

    /**
     * Get menu
     *
     * @return integer
     */
    public function getMenu()
    {
        return $this->menu;
    }
    public function __toString () 
    {
        return $this->getNombreSubmenu();
    }
}
