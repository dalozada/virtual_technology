<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Description of UsuarioRegistro
 *
 * @author dfmarin
 */
class UsuarioRegistro {
   
    /**
     * @var type 
     * @Assert\NotBlank(message="Por favor, escriba su identificacion")
     */
    protected $identificacion;
  
    
    
    /**
     * @Assert\Email(
     *     message = "El correo '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     */
    protected $email;
    
    /**
     * @var type 
     * @Assert\NotBlank(message="Por favor, escriba su primer nombre")
     */
    protected $nombre1;
   
    /**
     * @var type 
     * @Assert\NotBlank(message="Por favor, escriba su segundo nombre")
     */
    protected $nombre2;
    
    /**
     * @var type 
     * @Assert\NotBlank(message="Por favor, escriba su primer apellido")
     */
    protected $apellido1;
    
    /**
     * @var type 
     * @Assert\NotBlank(message="Por favor, escriba su segundo apellido")
     */
    protected $apellido2;  
 
    
    protected $fechaNac;
    
    /**
     * @var type 
     * @Assert\NotBlank(message="Por favor, escriba su direccion")
     */
    protected $direccion;   
    
    /**
     * @var type 
     * @Assert\NotBlank(message="Por favor, escriba su ciudad de nacimiento")
     */
    protected $ciudadNac;
     
    /**
     * @var type 
     * @Assert\NotBlank(message="Por favor, escriba su ciudad de residencia")
     */
    protected $ciudadRes;
    
    protected $usuario;
    
    protected $password;
    
    function getIdentificacion() {
        return $this->identificacion;
    }

    function getNombre1() {
        return $this->nombre1;
    }

    function getNombre2() {
        return $this->nombre2;
    }

    function getApellido1() {
        return $this->apellido1;
    }

    function getApellido2() {
        return $this->apellido2;
    }

    function getFechaNac() {
        return $this->fechaNac;
    }

    function getDireccion() {
        return $this->direccion;
    }

    function getEmail() {
        return $this->email;
    }

    function getCiudadNac() {
        return $this->ciudadNac;
    }

    function getCiudadRes() {
        return $this->ciudadRes;
    }

    function getUsuario() {
        return $this->usuario;
    }
    
    function getPassword() {
        return $this->password;
    }

    function setIdentificacion($identificacion) {
        $this->identificacion = $identificacion;
    }

    function setNombre1($nombre1) {
        $this->nombre1 = $nombre1;
    }

    function setNombre2($nombre2) {
        $this->nombre2 = $nombre2;
    }

    function setApellido1($apellido1) {
        $this->apellido1 = $apellido1;
    }

    function setApellido2($apellido2) {
        $this->apellido2 = $apellido2;
    }

    function setFechaNac($fechaNac) {
        $this->fechaNac = $fechaNac;
    }

    function setDireccion($direccion) {
        $this->direccion = $direccion;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setCiudadNac($ciudadNac) {
        $this->ciudadNac = $ciudadNac;
    }

    function setCiudadRes($ciudadRes) {
        $this->ciudadRes = $ciudadRes;
    }

    function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    function setPassword($password) {
        $this->password = $password;
    }

}
