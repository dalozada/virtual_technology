<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ciudad
 *
 * @ORM\Table(name="ciudad", indexes={@ORM\Index(name="ciudad_fk1", columns={"iddepartamento"})})
 * @ORM\Entity
 */
class Ciudad
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=10, nullable=true)
     */
    private $codigo;

    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Departamento")
    * @ORM\JoinColumn(name="iddepartamento", referencedColumnName="id")
    */
    private $departamento;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=50, nullable=true)
     */
    private $descripcion;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return Ciudad
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set departamento
     *
     * @param integer $departamento
     *
     * @return Ciudad
     */
    public function setdepartamento($departamento)
    {
        $this->departamento = $departamento;

        return $this;
    }

    /**
     * Get iddepartamento
     *
     * @return integer
     */
    public function getdepartamento()
    {
        return $this->departamento;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Ciudad
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    
    public function __toString () 
    {
        return $this->getCodigo();
    }
}
